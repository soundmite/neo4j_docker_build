# Neo4j Docker build.

Dockerfile and accompanying files to create a Docker image for the Neo4j Community Edition. 

To run a container:

	docker run -d --privileged --name neo soundmite/neo4j
	
Optionally add on a mapping for port 7474 to access from the host. For example:

	docker run -d -p 7474:7474 --privileged --name neo soundmite/neo4j

Run with a data volume to facilitate backup/portability of the data. For example:

	docker run -d -v /var/lib/neo4j/data --name neo-data busybox
	docker run -d --privileged --volumes-from neo-data --name neo soundmite/neo4j